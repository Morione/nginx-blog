package sk.morione.blog.loadbalancing.controller;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class Info {

    @Value( "${app.version}" )
    String appVersion;

    @Value( "${server.port}" )
    String serverPort;

    @GetMapping("/")
    String info() {  
        String text = String.format("App node is running on port %s. Version : %s", serverPort, appVersion);
        System.out.println(text);

        return text;
    }
}
